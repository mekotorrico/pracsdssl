﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Security.Authentication;

namespace Servidor
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Server s;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnEnviar_Click(object sender, RoutedEventArgs e)
        {
            //string res = "";

            //s = new Server("25.85.220.103", 4404);   //IPAddress.Parse("127.0.0.17")      //("localhost", 4404);
            //res = s.Start();
            //txtEstado.Text=(res);

            ///------------------------pr1   


            //string certificate = null;
            //certificate = "88010436_sdunivalle.com.cert";    // certificate = "88010436_sdunivalle.com.cert";
            //SslTcpServer.RunServer(certificate);

            ///------------------------pr2

            //X509Certificate cert2 = null;
            //cert2 = getServerCert();
            //SslTcpServer.RunServer(cert2);

            //---------------------pr3
         
            IPAddress ip = IPAddress.Parse("25.85.220.103");
            var serverCertificate = getServerCert();
            var listener = new TcpListener(ip, 4404);  
            listener.Start();

            while (true)
            {
                using (var client = listener.AcceptTcpClient())
                using (var sslStream = new SslStream(client.GetStream(),
                   false, ValidateCertificate))
                {
                    sslStream.AuthenticateAsServer(serverCertificate,
                       true, SslProtocols.Tls12, false);

                    var inputBuffer = new byte[4096];
                    var inputBytes = 0;
                    while (inputBytes == 0)
                    {
                        inputBytes = sslStream.Read(inputBuffer, 0,
                           inputBuffer.Length);
                    }
                    var inputMessage = Encoding.UTF8.GetString(inputBuffer,
                       0, inputBytes);
                   Console.WriteLine("Recepción: {0}", inputMessage);


                    string res = "";
                    s = new Server();
                   string mandarInfo = "Id Procesador: " + s.GetProcessorId() + "\n" + "MAC: " + s.GetMACAddress() + "\n" + "ID producto " + s.GetBoardProductId() + "\n" + "Bios: " + s.GetBIOSserNo() + "\n" + "Computadora: " + s.GetComputerName();
                    
                    res = Start();
                    txtEstado.Text = (res);

                    var outputMessage = mandarInfo;
                    var outputBuffer = Encoding.UTF8.GetBytes(outputMessage);
                    sslStream.Write(outputBuffer);
                    Console.WriteLine("Envio características: {0}", outputMessage);
                    

                }

            }
        }
        ///----pr

        static bool ValidateCertificate(Object sender, X509Certificate certificate, X509Chain chain,SslPolicyErrors sslPolicyErrors)
        {
            return true;
            if (sslPolicyErrors == SslPolicyErrors.None)
            { return true; }
            // we don't have a proper certificate tree
            if (sslPolicyErrors ==
                  SslPolicyErrors.RemoteCertificateChainErrors)
            { return true; }
            return false;
        }
        private static X509Certificate getServerCert()
        {
            X509Store store = new X509Store(StoreName.My,StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);

            X509Certificate2 foundCertificate = null;
            foreach (X509Certificate2 currentCertificate
               in store.Certificates)
            {
                if (currentCertificate.IssuerName.Name
                   != null && currentCertificate.IssuerName.
                   Name.Equals("CN=sdTdUnivalle"))
                {
                    foundCertificate = currentCertificate;
                    break;
                }
            }
            return foundCertificate;
        }

        public string Start()
        {
            string smsresultado = "";
            string mandarInfo = "";

            byte[] lista = new byte[1024];
            string sms;            //codificar la cadena de texto
            
            sms = Encoding.ASCII.GetString(lista);
            smsresultado = "Se recibió la ip: ";

            //  string machineName= host.HostName;
            // return smsresultado + sms; //1 op
          //  mandarInfo = "Id Procesador: " + GetProcessorId() + "\n" + "MAC: " + GetMACAddress() + "\n" + "ID producto " + GetBoardProductId() + "\n" + "Bios: " + GetBIOSserNo() + "\n" + "Computadora: " + GetComputerName();

            //byte[] respuesta = Encoding.ASCII.GetBytes(mandarInfo);
            //s_Cliente.Send(respuesta);

            return smsresultado;

        }
    }
}
