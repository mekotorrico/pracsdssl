﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cliente
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Client c;

        //--------pr3
        private static int _hostPort = 4404; 
        private static string _hostName = "25.85.220.103"; 
        private static string ServerCertificateName ="sdTdUnivalle";

        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void btnEnviar_Click(object sender, RoutedEventArgs e)
        {
            //string estado = "";
            //string sms; 
            //sms = txtIp.Text;

            //c = new Client("25.85.220.103", 4404);
            //c.Start();

            //estado = c.SendIP(sms);

            //txtEstado.Text = (estado);

            ////c = new Client("localhost", 4404);
            ////c.Start();
            ////while (true)
            ////{
            ////    sms = txtIp.Text;
            ////   // sms = Microsoft.VisualBasic.Interaction.InputBox("Ingrese mensaje (IP)");
            ////    estado = c.SendIP(sms);
            ////    txtEstado.Text=(estado);
            ////}
            //if (txtEstado.Text != "")
            //{
            //    txtInfo.Text = c.Recepcion();
            //}


            //------------------funciona
            //string serverCertificateName = "localhost";    //"localhost"   o nombre certificado?
            //string machineName = null;

            //// User can specify the machine name and server name.
            //// Server name must match the name on the server's certificate.
            //machineName = "localhost";

            //SslTcpClient.RunClient(machineName, serverCertificateName);

            //--------------pr3
            var clientCertificate = getServerCert();
            var clientCertificateCollection = new X509CertificateCollection(new X509Certificate[] { clientCertificate });

            using (var client = new TcpClient(_hostName, _hostPort))
            using (var sslStream = new SslStream(client.GetStream(),false, ValidateCertificate))
            {
                sslStream.AuthenticateAsClient(ServerCertificateName, clientCertificateCollection, SslProtocols.Tls12, false);

                var outputMessage = "Datos seguros";
                var outputBuffer = Encoding.UTF8.GetBytes(outputMessage);
                sslStream.Write(outputBuffer);
                Console.WriteLine("Envio: {0}", outputMessage);

                var inputBuffer = new byte[4096];
                var inputBytes = 0;

                inputBytes = sslStream.Read(inputBuffer, 0, inputBuffer.Length);
                var entrada = Encoding.UTF8.GetString(inputBuffer, 0, inputBytes);
               // Console.WriteLine("Recepcion: {0}", entrada);
                txtInfo.Text = ("Recepcion: "+"\n"+ entrada);
                txtEstado.Text = "Conexión Segura";
            }

        }

        //-----------pr3
        static bool ValidateCertificate(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
            { return true; }
            // ignore chain errors as where self signed
            if (sslPolicyErrors == SslPolicyErrors.RemoteCertificateChainErrors)
            { return true; }
            return false;
        }
        private static X509Certificate getServerCert()
        {
            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);

            X509Certificate2 foundCertificate = null;
            foreach (X509Certificate2 currentCertificate in store.Certificates)
            {
                if (currentCertificate.IssuerName.Name
                   != null && currentCertificate.IssuerName.
                   Name.Equals("CN=sdTdUnivalle"))
                {
                    foundCertificate = currentCertificate;
                    break;
                }
            }
            return foundCertificate;
        }



    }
}
